import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { DummyComponent } from './components/dummy.component';

export const routes: Routes = [
  {
    path: `main`,
    component: DummyComponent,
  },
  {
    path: 'feature1',
    loadChildren: () => import('./feature1/feature1.module').then(m => m.Feature1Module)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}

