import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-feature1',
  template: `<router-outlet></router-outlet>`,
})
export class Feature1Component {

}
