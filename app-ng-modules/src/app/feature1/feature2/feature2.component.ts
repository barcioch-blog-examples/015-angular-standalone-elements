import { Component } from '@angular/core';

@Component({
  selector: 'app-feature2',
  template: `
    <router-outlet></router-outlet>`,
})
export class Feature2Component {

}
