import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { Feature1Component } from './feature1.component';

export const routes: Routes = [
  {
    path: ``,
    component: Feature1Component,
    children: [
      {
        path: 'feature2',
        loadChildren: () => import('./feature2/feature2.module').then(m => m.Feature2Module)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Feature1RoutingModule {
}
