import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TestIdDirective } from './test-id.directive';

describe('TestIdDirective', () => {
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
  });

  describe('when component with directive is initialized', () => {
    it('should add data-test-id attribute to the element', () => {
      expect(getElement().nativeElement.getAttribute('data-test-id')).toBe('aaa-bb-1');
    });
  });

  const getElement = (): DebugElement => {
    return fixture.debugElement.query(By.css('[data-test="element-with-id"]'));
  }
});


@Component({
  selector: 'app-test',
  standalone: true,
  imports: [TestIdDirective],
  template: `
    <div data-test="element-with-id" test-id></div>`,
})
export class TestComponent {

}
