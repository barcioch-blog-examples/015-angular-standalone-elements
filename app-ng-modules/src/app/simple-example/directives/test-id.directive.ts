import { Directive, HostBinding, HostListener, inject } from '@angular/core';
import { Logger } from '../services/logger';

@Directive({
  selector: '[test-id]',
  standalone: true,
})
export class TestIdDirective {
  @HostBinding("attr.data-test-id") testId = 'aaa-bb-1';
}
