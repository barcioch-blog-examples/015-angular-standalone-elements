import { ReverseTextPipe } from './reverse-text.pipe';

describe('ReverseTextPipe', () => {
  describe('when pipe is created', () => {
    let pipe: ReverseTextPipe;

    beforeAll(() => {
      pipe = new ReverseTextPipe();
    });

    it.each([
      { input: undefined, text: 'undefined' },
      { input: null, text: 'null' },
      { input: '', text: 'empty string' },
    ])('should transform $text to empty string', ({ input, text }) => {
      expect(pipe.transform(input)).toBe('');
    });

    it('should reverse the "reverse me" text to "em esrever"', () => {
      expect(pipe.transform('reverse me')).toBe('em esrever');
    });
  });
});
