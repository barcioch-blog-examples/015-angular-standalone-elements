import { Routes } from '@angular/router';
import { DummyComponent } from './components/dummy.component';

export const routes: Routes = [
  {
    path: `main`,
    component: DummyComponent,
  },
  {
    path: 'feature1',
    loadChildren: () => import('./feature1/feature1-routes').then(r => r.routes)
  }
];
