import { TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingHarness } from '@angular/router/testing';
import { provideRouter } from '@angular/router';
import { routes } from './app-routes';


describe('App Routing', () => {
  let harness: RouterTestingHarness;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [provideRouter(routes)]
    });

    harness = await RouterTestingHarness.create();
  });

  describe('when testing module is initialized', () => {
    describe('and user enters "/main" route', () => {
      beforeEach(async () => {
        await harness.navigateByUrl('/main');
      });

      it('should render DummyComponent', () => {
        const element = harness.fixture.debugElement.query(By.css('app-dummy'));
        expect(element).toBeTruthy()
      });
    });

    describe('and user enters "/feature1" route', () => {
      beforeEach(async () => {
        await harness.navigateByUrl('/feature1');
      });

      it('should render Feature1Component', () => {
        const element = harness.fixture.debugElement.query(By.css('app-feature1'));
        expect(element).toBeTruthy()
      });
    });

    describe('and user enters "/feature1/feature2" route', () => {
      beforeEach(async () => {
        await harness.navigateByUrl('/feature1/feature2');
      });

      it('should render Feature1Component', () => {
        const element = harness.fixture.debugElement.query(By.css('app-feature1'));
        expect(element).toBeTruthy()
      });

      it('should render Feature2Component', () => {
        const element = harness.fixture.debugElement.query(By.css('app-feature2'));
        expect(element).toBeTruthy()
      });
    });
  });
});
