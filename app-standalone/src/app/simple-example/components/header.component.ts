import { Component } from '@angular/core';

@Component({
  selector: 'app-h1',
  standalone: true,
  template: `
    <h1 data-test="header-component">
      <ng-content></ng-content>
    </h1>`,
})
export class HeaderComponent {

}
