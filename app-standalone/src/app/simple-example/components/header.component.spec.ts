import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
  });

  describe('when component is initialized', () => {
    it('should display header component', () => {
      expect(getElement()).toBeTruthy();
    });

    it('should display header component content', () => {
      expect(getElement().nativeElement.textContent).toBe('the content');
    });
  });

  const getElement = (): DebugElement => {
    return fixture.debugElement.query(By.css('[data-test="header"]'));
  }
});


@Component({
  selector: 'app-test',
  standalone: true,
  imports: [HeaderComponent],
  template: `
    <app-h1 data-test="header">the content</app-h1>`,
})
export class TestComponent {

}
