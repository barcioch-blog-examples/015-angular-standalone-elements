import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "reverseText",
  standalone: true,
})
export class ReverseTextPipe implements PipeTransform {
  transform(value: string | undefined | null): string {
    if (value === null || value === undefined) {
      return '';
    }

    return value.split('').reverse().join('');
  }
}
