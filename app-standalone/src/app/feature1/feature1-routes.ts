import { Routes } from '@angular/router';
import { Feature1Component } from './feature1.component';

export const routes: Routes = [
  {
    path: ``,
    component: Feature1Component,
    children: [
      {
        path: 'feature2',
        loadChildren: () => import('./feature2/feature2-routes').then(r => r.routes)
      }
    ]
  },
];
