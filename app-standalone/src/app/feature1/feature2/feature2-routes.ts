import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { Feature2Component } from './feature2.component';

export const routes: Routes = [
  {
    path: ``,
    component: Feature2Component,
  },
];
